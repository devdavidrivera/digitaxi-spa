import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {

  public data: any;
  public password: any;
  constructor(private authservice: AuthenticationService, private router: Router, private route: ActivatedRoute) {
    this.data = {
      'username' : ''
    };
  }

  ngOnInit() {
  }

  onSubmit() {

    this.authservice.consult(this.data)
      .subscribe(
        result => {

       if (result.length !== 0) {
         if (result[0].password === this.password) {

           localStorage.setItem('user', this.data.username);
           this.router.navigate(['home']);
         }else {
           console.log('Usuario o Contraseña inválida');
           console.log(result[0].password);
         }
        }else {
         console.log('Usuario o Contraseña inválida');
         }
        }, err => {
          console.log('err', err);
        }
      );
  }
}
