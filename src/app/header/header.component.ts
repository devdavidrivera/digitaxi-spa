import {Component, Input, OnInit} from '@angular/core';
declare var $ :any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() onMenuButtonClick: () => void;

  constructor() {
  }

  ngOnInit() {
    $(document).ready(function () {
      $('[data-toggle="popover"]').popover();
    });

  }
}
