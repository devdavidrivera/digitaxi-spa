import {AfterViewInit, Component, DoCheck, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
declare var jQuery:any;
declare var $:any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, DoCheck, AfterViewInit{


  @ViewChild('slide') slide: any;
  @ViewChild('link') link: ElementRef;
  @ViewChild('videoContainer') videoContainer: ElementRef;
  @ViewChild('video') video: ElementRef;
  @ViewChild('close') close: ElementRef;


   constructor(private titleService: Title, private metaService: Meta) {

  }

  ngOnInit() {
    this.play();
    this.setTitle('Somos Digitaxi - Aplicación homologada por el Ministerio de Transporte');
    const so = navigator.platform ;
    if ( so.startsWith('Android') || so.startsWith('Linux')) {
      this.link.nativeElement.setAttribute('href','https://play.google.com/store/apps/details?id=com.digitaxi.user');
    }
    else if ( so.startsWith('iPhone') || so.startsWith('iPod') || so.startsWith('iPad')) {
      this.link.nativeElement.setAttribute('href','https://itunes.apple.com/co/app/digi/id1038468245?mt=8');
    }
  }

  ngAfterViewInit() {
    $( document ).ready(function() {
      //  setTimeout(()=>{  this.video.nativeElement.play(); }, 11000);

      this.video.nativeElement.play();
    }.bind(this));
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
    this.metaService.updateTag({
        content: 'DigiTaxi - Ingresa y conoce todo lo que' +
        'puedes hacer con la mejor aplicación para taxi. Plataforma tecnológica avalada por decreto 2163'
      },
      'name="Description"'
    );
  }

  ngDoCheck() {
    // this.video.nativeElement.play();
      $('video').on('ended', function(){
        this.closeWindow();
      }.bind(this));

  }

  closeWindow() {
    this.videoContainer.nativeElement.setAttribute('class','change-display');
    this.close.nativeElement.setAttribute('class','change-display');
    this.video.nativeElement.pause();
    this.video.nativeElement.src = '';
    this.video.nativeElement.load();

  }

  play(){
    this.video.nativeElement.play();
  }
}
