import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { SendmailService } from '../../services/sendmail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css'],
  providers: [SendmailService]
})
export class ContactoComponent implements OnInit {

  public user: any;
  @ViewChild ('link2') link: ElementRef;
  @ViewChild('myModal') myModal: any;
  private DOM: any;

  constructor(private sendmail: SendmailService, private router: Router, private route: ActivatedRoute,
              private titleService: Title, private metaService: Meta) {
    this.user = {
      'from': 'Digitaxi',
      'email': '' ,
      'subject': '' ,
      'body': '',
      'name': '',
      'phone': '',
      'recipients': 'johan@digimarketing.co, natalia@digimarketing.co'
   };
  }
  ngOnInit() {

    this.setTitle('Mejor Plataforma Tecnológica avalada por el Decreto 2163 es Digitaxi');

  }
  onSubmit() {

     this.sendmail.send(this.user)
      .subscribe(
         result => {
            console.log('result', result);
        }, err => {
          console.log('err', err);
        }
        );
    this.clear();
    this.router.navigate(['send'], { relativeTo: this.route });
  }
   public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
     this.metaService.updateTag({
         content: 'Empiece a utilizar una aplicación homologada por el Ministerio de Transporte.' +
         ' Remplace su viejo taxímetro por Digitaxi. Contáctenos ahora para más información.'
       },
       'name="Description"'
     );
  }
  downloadAll() {

    this.link.nativeElement.setAttribute('href',  ' ./assets/docs/RES 3815 - 2017 PLATAFORMA DIGITAXI.pdf');
    this.link.nativeElement.setAttribute('download',  './assets/docs/RES 3815 - 2017 PLATAFORMA DIGITAXI.pdf');
    this.link.nativeElement.click();

  }
  clear() {
    this.user = {
      'email': '',
      'subject': '',
      'message': '',
      'name': '',
      'phone': ''
    };
  }


}
