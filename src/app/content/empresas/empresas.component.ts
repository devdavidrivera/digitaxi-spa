import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {


  @ViewChild ('link2') link: ElementRef;

  constructor(private titleService: Title, private metaService: Meta) { }

  ngOnInit() {
    this.setTitle('Convierta su empresa en uno de los socios de la mejor aplicación');
  }
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
    this.metaService.updateTag({
        content: 'Únete a DigiTaxi y disfruta una de las gran aplicaciones ' +
        'diseñada para los taxistas. Ingresa y conoce la mejor opción para reservar carreras.'
      },
      'name="Description"'
    );
  }

  downloadAll() {

    this.link.nativeElement.setAttribute('href',  ' ./assets/docs/RES 3815 - 2017 PLATAFORMA DIGITAXI.pdf');
    this.link.nativeElement.setAttribute('download',  './assets/docs/RES 3815 - 2017 PLATAFORMA DIGITAXI.pdf');
    this.link.nativeElement.click();

  }


  }

