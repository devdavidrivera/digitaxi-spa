import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
declare var titlePage: string;
declare var descPage: string;
declare var $:any;

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class NosotrosComponent implements OnInit {

  constructor(private titleService: Title, private metaService: Meta) {}

  ngOnInit() {
    this.setTitle('¿Ya conoce a Digitaxi? Aplicación homologada por el Ministerio');
  }
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
    this.metaService.updateTag({
        content: 'Somos Digitaxi una aplicación homologada' +
        'por el Ministerio de Transporte y buscamos reunir a los mejores taxistas con los mejores usuarios. Conócenos ya.'
      },
      'name="Description"'
    );
  }
}
