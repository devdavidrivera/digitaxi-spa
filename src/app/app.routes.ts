
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NosotrosComponent} from './content/nosotros/nosotros.component';
import {ContactoComponent} from './content/contacto/contacto.component';
import {HomeComponent} from './content/home/homecomponent';
import {TaxistasComponent} from './content/taxistas/taxistas.component';
import {EmpresasComponent} from './content/empresas/empresas.component';
import {SendComponent} from './content/contacto/send/send.component';
import {LoginComponent} from "./cms/login/login.component";
import {ContentComponent} from "./content/content.component";

// Route Configuration
export const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo:'home', pathMatch: 'full' },
  { path: 'nosotros', component: NosotrosComponent },
  { path: 'home/nosotros', redirectTo:'nosotros' },
  { path: 'contacto', component: ContactoComponent },
  { path: 'home/contacto', redirectTo:'contacto' },
  { path: 'empresas',  component: EmpresasComponent },
  { path: 'home/empresas', redirectTo:'empresas' },
  // { path: 'taxistas', component: TaxistasComponent },
  { path: 'contacto/send', component: SendComponent },
  // { path: 'login', component: LoginComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
