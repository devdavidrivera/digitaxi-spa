import { BrowserModule, Title, Meta } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuHeaderComponent } from './header/menu-header/menu-header.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './content/home/homecomponent';
import { routing } from './app.routes';
import { NosotrosComponent } from './content/nosotros/nosotros.component';
import { ContactoComponent } from './content/contacto/contacto.component';
import { EmpresasComponent } from './content/empresas/empresas.component';
import { TaxistasComponent } from './content/taxistas/taxistas.component';
import {FormsModule} from '@angular/forms';
import { SendComponent } from './content/contacto/send/send.component';
import { CmsComponent } from './cms/cms.component';
import { LoginComponent } from './cms/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuHeaderComponent,
    ContentComponent,
    FooterComponent,
    HomeComponent,
    NosotrosComponent,
    ContactoComponent,
    EmpresasComponent,
    TaxistasComponent,
    SendComponent,
    CmsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  entryComponents: [
    HomeComponent,
    ContactoComponent,
    NosotrosComponent
],
  providers:  [Title, Meta],
  bootstrap: [AppComponent]
})
export class AppModule { }
