import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {RequestOptions} from '@angular/http';


@Injectable()
export class AuthenticationService {

  constructor(private _http: Http) { }

  consult(object) {
    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    const options = new RequestOptions({ headers: headers });
    return this._http.post('https://digitaxi.co/php/restService/auth', object, options)
      .map(res => res.json());
  }
  auth(object) {
   const result = this.consult(object);
     console.log(result);
     return result;
    }

  //   const result = this.consult(obj);
  //   console.log(result);
  //   // localStorage.setItem('username', username);
  //   // localStorage.setItem('login', 'true');
  //   // this.router.navigate(['home']);
  // }

}
