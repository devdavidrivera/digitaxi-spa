import { Injectable } from '@angular/core';
import { Http,  Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {RequestOptions} from '@angular/http';


@Injectable()
export class SendmailService {
  constructor(private _http: Http) {}

  send(object) {

    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    const options = new RequestOptions({ headers: headers });

    // return this._http.post('http://devtaxi.digimas.co/api/v2/mail/send', object, options)
    return this._http.post('https://digitaxi.co/php/restService/auto', object, options)
      .map(res => res.json());
  }
}
